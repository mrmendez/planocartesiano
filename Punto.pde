class Punto{
   float x;
   float y;
   float diametro;
   
   Punto(float a, float b){
       x = a;
       y = b;
       diametro = 3;
       ellipseMode(CENTER);
   }
   
   void dibujaPunto(){
      fill(255,0,0);
      stroke(255,0,0);
      ellipse(x,y,diametro, diametro);
   }
}
