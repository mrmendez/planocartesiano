class Circulo{
   float h;  // Posición del apuntador del mouse en la ventana
   float k;
   float radio;
   float posX; //Pano cartesiano
   float posY;
   Circulo(float a, float b, float c){
      h = a;
      k = b;
      radio = c;
      ellipseMode(CENTER);
   }
   void dibujaCirculo(){
       stroke(0,0,255);
       strokeWeight(1);
       noFill();
       ellipse(h,k,2*radio, 2*radio);
   }    
   boolean isAdentro(float a, float b){
      return dist(h,k,a,b) <= radio;
   }
}
